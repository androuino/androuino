/**
 * L298N Dual H-Bridge DC Motor Driver
 *
 * l298nSketch.ino
 */
#include <Servo.h>
#include <SoftwareSerial.h>

#define analogPin 10

// create servo
Servo myservo;
SoftwareSerial esp(5,6);
int pos = 45;
const int pinForward = 3; // pin 2 of L298N Driver
const int pinReverse = 4;  // pin 7 of L298N Driver
const int BTState = 7;
const int Horn = 8;
const int Headlight = 9;
const int Door = 11;

// how much serial data we expect before a newline
const unsigned int MAX_INPUT = 50;


// byte to receive from android
byte cmd;
char c[8];
int motorSpeed = 0;

const char startOfNumberDelimiter = '<';
const char endOfNumberDelimiter   = '>';

void setup ()
{ 
  Serial.begin (9600);
  esp.begin(9600);
  myservo.attach(9);

  // setup pin mode
  pinMode(analogPin, OUTPUT);
  pinMode(pinForward, OUTPUT);
  pinMode(pinReverse, OUTPUT);
  pinMode(BTState, INPUT);
  pinMode(Horn, OUTPUT);
  pinMode(Headlight, OUTPUT);
  pinMode(Door, OUTPUT);
} // end of setup
  
void processNumber(const long n)
{
  Serial.println(n);
  pos = (int)n;
  myservo.write(pos);
}  // end of processNumber

// here to process incoming serial data after a terminator received
void process_data(const char * data)
{
  // for now just display it
  // (but you could compare it to some value, convert to an integer, etc.)
  Serial.print("process_data function:");
  Serial.println(data);

  memset(c, '\0', sizeof(c));
  strncpy(c, data, sizeof(c));

  if (strcmp(c, "A") == 0)      motorSpeed = 100;
  else if (strcmp(c, "B") == 0) motorSpeed = 150;
  else if (strcmp(c, "C") == 0) motorSpeed = 200;
  else if (strcmp(c, "D") == 0) motorSpeed = 255;

  // Forward command
  if (strcmp(c, "F") == 0)
  {
    digitalWrite(pinForward, HIGH);
    digitalWrite(pinReverse, LOW);
  }
  // Reverse command
  else if (strcmp(c, "G") == 0)
  {
    digitalWrite(pinForward, LOW);
    digitalWrite(pinReverse, HIGH);
  }
  // STOP
  else if (strcmp(c, "S") == 0)
  {
    digitalWrite(pinForward, LOW);
    digitalWrite(pinReverse, LOW);
  }
  // RIGHT
  else if (strcmp(c, "R") == 0)
  {
    pos = 90;
    myservo.write(pos);
  }
  // LEFT
  else if (strcmp(c, "L") == 0)
  {
    pos = 0;
    myservo.write(pos);
  }
  // STRAIGHT
  else if (strcmp(c, "I") == 0)
  {
    pos = 45;
    myservo.write(pos);
  }
  // Enable Horn
  else if (strcmp(c, "H") == 0)
  {
    digitalWrite(Horn, HIGH);
  }
  // Disable Horn
  else if (strcmp(c, "J") == 0)
  {
    digitalWrite(Horn, LOW);
  }
  // Enable Headlight
  else if (strcmp(c, "K") == 0)
  {
    digitalWrite(Headlight, HIGH);
  }
  // Disable Headlight
  else if (strcmp(c, "P") == 0)
  {
    digitalWrite(Headlight, LOW);
  }
  // Enable door
  else if (strcmp(c, "Q") == 0)
  {
    digitalWrite(Door, HIGH);
  }
  // Disable door
  else if (strcmp(c, "U") == 0)
  {
    digitalWrite(Door, LOW);
  }
}  // end of process_data

void processInput()
{
  static long receivedNumber = 0;
  static boolean negative = false;

  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  byte c = Serial.read ();

  switch (c)
  {

    case endOfNumberDelimiter:
      if (negative)
        processNumber (- receivedNumber);
      else
        processNumber (receivedNumber);

    // fall through to start a new number
    case startOfNumberDelimiter:
      receivedNumber = 0;
      negative = false;
      break;

    case '0' ... '9':
      receivedNumber *= 10;
      receivedNumber += c - '0';
      break;

    case '-':
      negative = true;
      break;

    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data (input_line);

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = c;
      break;
  } // end of switch
}  // end of processInput

void processInput2()
{
  static long receivedNumber = 0;
  static boolean negative = false;

  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  byte c = esp.read();

  switch (c)
  {

    case endOfNumberDelimiter:
      if (negative)
        processNumber (- receivedNumber);
      else
        processNumber (receivedNumber);

    // fall through to start a new number
    case startOfNumberDelimiter:
      receivedNumber = 0;
      negative = false;
      break;

    case '0' ... '9':
      receivedNumber *= 10;
      receivedNumber += c - '0';
      break;

    case '-':
      negative = true;
      break;

    case '\n':   // end of text
      input_line[input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data(input_line);

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = c;
      break;
  } // end of switch
}  // end of processInput

void loop ()
{

  if (Serial.available())
    processInput();
  else if (esp.available())
    processInput2();

  myservo.write(pos);
  motorSpeed = 100;
  // do other stuff here
} // end of loop


/**
 * IBT-3/IBT-4 Module Brushed Motor Driver
 *
 * IBT-3Sketch.ino
 */
#include <Servo.h>
#include <SoftwareSerial.h>

// create servo
Servo myservo;
#define rx 5
#define tx 6
#define pinForward 3 // pin 2 of L298N Driver
#define pinReverse 11  // pin 7 of L298N Driver
SoftwareSerial esp(rx,tx); // rx, tx
const int BTState = 7;
const int Horn = 8;
const int Headlight = 9;
const int Door = 10;

// how much serial data we expect before a newline
const unsigned int MAX_INPUT = 50;


// byte to receive from android
byte cmd;
char c[8];
int motorSpeed = 0;
int pos = 45;

const char startOfNumberDelimiter = '<';
const char endOfNumberDelimiter   = '>';

void setup ()
{
  Serial.begin (9600);
  esp.begin(9600);
  myservo.attach(9);
  myservo.write(pos);
  motorSpeed = 100;

  // setup pin mode
  pinMode(pinForward, OUTPUT);
  pinMode(pinReverse, OUTPUT);
  pinMode(BTState, INPUT);
  pinMode(Horn, OUTPUT);
  pinMode(Headlight, OUTPUT);
  pinMode(Door, OUTPUT);

  digitalWrite(pinForward, LOW);
  digitalWrite(pinReverse, LOW);
} // end of setup

void processNumber(const long n)
{
  Serial.println(n);
  pos = (int)n;
  myservo.write(pos);
}  // end of processNumber

// here to process incoming serial data after a terminator received
void process_data(const char * data)
{
  // for now just display it
  // (but you could compare it to some value, convert to an integer, etc.)
  Serial.print("process_data function:");
  //Serial.println(data);

  memset(c, '\0', sizeof(c));
  strncpy(c, data, sizeof(c));
  Serial.println(c);

  if (strcmp(c, "A") == 0)
    motorSpeed = 100;
  else if (strcmp(c, "B") == 0)
    motorSpeed = 150;
  else if (strcmp(c, "C") == 0)
    motorSpeed = 200;
  else if (strcmp(c, "D") == 0)
    motorSpeed = 252;

  // Forward command
  if (strcmp(c, "F") == 0)
  {
    digitalWrite(pinReverse, LOW);
    analogWrite(pinForward, motorSpeed);
  }
  // Reverse command
  else if (strcmp(c, "G") == 0)
  {
    digitalWrite(pinForward, LOW);
    analogWrite(pinReverse, motorSpeed);
  }
  // STOP
  else if (strcmp(c, "S") == 0)
  {
    digitalWrite(pinForward, LOW);
    digitalWrite(pinReverse, LOW);
  }
  // RIGHT
  else if (strcmp(c, "R") == 0)
  {
    pos = 90;
    myservo.write(pos);
  }
  // LEFT
  else if (strcmp(c, "L") == 0)
  {
    pos = 0;
    myservo.write(pos);
  }
  // STRAIGHT
  else if (strcmp(c, "I") == 0)
  {
    pos = 45;
    myservo.write(pos);
  }
  // Enable Horn
  else if (strcmp(c, "H") == 0)
  {
    digitalWrite(Horn, HIGH);
  }
  // Disable Horn
  else if (strcmp(c, "J") == 0)
  {
    digitalWrite(Horn, LOW);
  }
  // Enable Headlight
  else if (strcmp(c, "K") == 0)
  {
    digitalWrite(Headlight, HIGH);
  }
  // Disable Headlight
  else if (strcmp(c, "P") == 0)
  {
    digitalWrite(Headlight, LOW);
  }
  // Enable door
  else if (strcmp(c, "Q") == 0)
  {
    digitalWrite(Door, HIGH);
  }
  // Disable door
  else if (strcmp(c, "U") == 0)
  {
    digitalWrite(Door, LOW);
  }
}  // end of process_data

void processInput()
{
  static long receivedNumber = 0;
  static boolean negative = false;

  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  byte c = Serial.read();

  switch (c)
  {

    case endOfNumberDelimiter:
      if (negative)
        processNumber (- receivedNumber);
      else
        processNumber (receivedNumber);

    // fall through to start a new number
    case startOfNumberDelimiter:
      receivedNumber = 0;
      negative = false;
      break;

    case '0' ... '9':
      receivedNumber *= 10;
      receivedNumber += c - '0';
      break;

    case '-':
      negative = true;
      break;

    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data (input_line);

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = c;
      break;
  } // end of switch
}  // end of processInput

void processInput2()
{
  static long receivedNumber = 0;
  static boolean negative = false;

  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;

  byte c = (byte) esp.read();

  switch (c)
  {

    case endOfNumberDelimiter:
      if (negative)
        processNumber (- receivedNumber);
      else
        processNumber (receivedNumber);

    // fall through to start a new number
    case startOfNumberDelimiter:
      receivedNumber = 0;
      negative = false;
      break;

    case '0' ... '9':
      receivedNumber *= 10;
      receivedNumber += c - '0';
      break;

    case '-':
      negative = true;
      break;

    case '\n':   // end of text
      input_line[input_pos] = 0;  // terminating null byte

      // terminator reached! process input_line here ...
      process_data(input_line);

      // reset buffer for next time
      input_pos = 0;
      break;

    case '\r':   // discard carriage return
      break;

    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = c;
      break;
  } // end of switch
}  // end of processInput2

void loop ()
{
  if (Serial.available() > 0)
  {
    processInput();
  }
  else if (esp.available() > 0)
  {
    processInput2();
  }

} // end of loop