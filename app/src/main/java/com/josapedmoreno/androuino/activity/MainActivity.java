/**
 * MainActivity.java by Josaped Moreno
 */

package com.josapedmoreno.androuino.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.appszoom.appszoomsdk.AppsZoom;
import com.josapedmoreno.androuino.R;
import com.josapedmoreno.androuino.adapter.NavDrawerListAdapter;
import com.josapedmoreno.androuino.fragments.FragmentAbout;
import com.josapedmoreno.androuino.fragments.FragmentArduinoCode;
import com.josapedmoreno.androuino.fragments.FragmentBug;
import com.josapedmoreno.androuino.fragments.FragmentFly;
import com.josapedmoreno.androuino.fragments.FragmentHomeAutomation;
import com.josapedmoreno.androuino.fragments.FragmentWifi;
import com.josapedmoreno.androuino.model.NavDrawerItem;

import java.util.ArrayList;

//import android.support.v4.app.Fragment;

/**
 * Created by Josaped Moreno on 8/5/2015.
 */
public class MainActivity extends FragmentActivity {

    public static final String TAG = MainActivity.class.getSimpleName();
    public static final String MY_PUBLISHER_KEY = "cbcc9432-d88a-4075-afd7-8ff142aba1de";
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    Fragment fragment = null;

    // navigation drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        final Activity activity = this;

       /* AdBuddiz.setLogLevel(AdBuddizLogLevel.Info);
        AdBuddiz.setPublisherKey(MY_PUBLISHER_KEY);
        AdBuddiz.setTestModeActive();
        AdBuddiz.cacheAds(activity);
        AdBuddiz.showAd(activity);*/
        AppsZoom.start(this);
        AppsZoom.showAd(this);

        mTitle = mDrawerTitle = getTitle();

        // load menu items
        navMenuTitles = getResources().getStringArray(R.array.sliding_menu);

        // load menu icons
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icon);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        //****************** ADDING NAVIGATION ITEMS ********************//
        // ANDROUINO FLY
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        // ANDROUINO WHEELS
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // ANDROUINO HOME
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        // ARDUINO
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        // BUG
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        // ABOUT
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));

        // Recycle the typed array
        navMenuIcons.recycle();

        // Set click listener
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // set the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);

        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_menu, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }
    }

    private void displayView(int position) {
        // update the main content by replacing fragments
        switch (position) {
            case 0:
                fragment = new FragmentFly();
                break;
            case 1:
                fragment = new FragmentWifi();
                Toast.makeText(this, "Use Volume Up Key for \"HORN\"", Toast.LENGTH_LONG).show();
                break;
            case 2:
                fragment = new FragmentHomeAutomation();
                break;
            case 3:
                fragment = new FragmentArduinoCode();
                break;
            case 4:
                fragment = new FragmentBug();
                break;
            case 5:
                fragment = new FragmentAbout();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer;
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else if (fragment == null) {
            // error loading fragment
            Log.d(TAG, "Error loading fragment!");
        }
    }

    private class SlideMenuClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            // displays view for selected item
            displayView(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar icon
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // handles action bar click event
        switch (item.getItemId()) {
            case R.id.action_settings:
                finish();
                return true;
            case R.id.edit_values:
                Intent intent = new Intent(this, EditActivity.class);
                startActivityForResult(intent, 4);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is open, hide the action items
        boolean draweropen = mDrawerLayout.isDrawerOpen(mDrawerList);
        MenuItem actionSettings = menu.findItem(R.id.action_settings).setVisible(!draweropen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
        //AdBuddiz.onDestroy();
    }
}