package com.josapedmoreno.androuino.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.DigitsKeyListener;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.josapedmoreno.androuino.R;

/**
 * Created by josapedmoreno on 5/23/16.
 */
public class EditActivity extends Activity {

    private static final String TAG = EditActivity.class.getSimpleName();
    private static final String FIRST = "FIRST";
    private static final String SECOND = "SECOND";
    private static final String THIRD = "THIRD";
    private static final String FOURTH = "FOURTH";
    private static final String DRIVE = "DRIVE";
    private static final String REVERSE = "REVERSE";
    private static final String STOP = "STOP";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";
    private static final String STRAIGHT = "STRAIGHT";
    private static final String LIGHTS_ON = "LIGHTS_ON";
    private static final String LIGHTS_OFF = "LIGHTS_OFF";
    private static final String DOOR_OPEN = "DOOR_OPEN";
    private static final String DOOR_CLOSE = "DOOR_CLOSE";
    private static final String HORN_ON = "HORN_ON";
    private static final String HORN_OFF = "HORN_OFF";
    private static final String MIN = "MIN";
    private static final String MAX = "MAX";
    private static int servoPos = 0;

    // UI interface
    private Button save, reset;

    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.edit_values);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();

        final EditText etFirst = (EditText) findViewById(R.id.etFirst);
        final EditText etSecond = (EditText) findViewById(R.id.etSecond);
        final EditText etThird = (EditText) findViewById(R.id.etThird);
        final EditText etFourth = (EditText) findViewById(R.id.etFourth);
        final EditText etDrive = (EditText) findViewById(R.id.etDrive);
        final EditText etReverse = (EditText) findViewById(R.id.etReverse);
        final EditText etStop = (EditText) findViewById(R.id.etStop);
        final EditText etLeft = (EditText) findViewById(R.id.etLeft);
        final EditText etRight = (EditText) findViewById(R.id.etRight);
        final EditText etStraight = (EditText) findViewById(R.id.etStraight);
        final EditText etLightsOff = (EditText) findViewById(R.id.etLightsOff);
        final EditText etLightsOn = (EditText) findViewById(R.id.etLightsOn);
        final EditText etDoorOpen = (EditText) findViewById(R.id.etDoorOpen);
        final EditText etDoorClose = (EditText) findViewById(R.id.etDoorClose);
        final EditText etHornOn = (EditText) findViewById(R.id.etHornOn);
        final EditText etHornOff = (EditText) findViewById(R.id.etHornOff);
        final EditText etMin = (EditText) findViewById(R.id.etMin);
        final EditText etMax = (EditText) findViewById(R.id.etMax);
        save = (Button) findViewById(R.id.buttonOk);
        reset = (Button) findViewById(R.id.buttonReset);

        etMax.setKeyListener(DigitsKeyListener.getInstance("1234567890"));
        etMin.setEnabled(false);

        etFirst.setText(sharedPreferences.getString(FIRST, "A"));
        etSecond.setText(sharedPreferences.getString(SECOND, "B"));
        etThird.setText(sharedPreferences.getString(THIRD, "C"));
        etFourth.setText(sharedPreferences.getString(FOURTH, "D"));
        etDrive.setText(sharedPreferences.getString(DRIVE, "F"));
        etReverse.setText(sharedPreferences.getString(REVERSE, "G"));
        etStop.setText(sharedPreferences.getString(STOP, "S"));
        etLeft.setText(sharedPreferences.getString(LEFT, "L"));
        etRight.setText(sharedPreferences.getString(RIGHT, "R"));
        etStraight.setText(sharedPreferences.getString(STRAIGHT, "I"));
        etLightsOff.setText(sharedPreferences.getString(LIGHTS_OFF, "K"));
        etLightsOn.setText(sharedPreferences.getString(LIGHTS_ON, "P"));
        etDoorOpen.setText(sharedPreferences.getString(DOOR_OPEN, "Q"));
        etDoorClose.setText(sharedPreferences.getString(DOOR_CLOSE, "U"));
        etHornOn.setText(sharedPreferences.getString(HORN_ON, "H"));
        etHornOff.setText(sharedPreferences.getString(HORN_OFF, "J"));
        etMin.setText(Integer.toString(sharedPreferences.getInt(MIN, 0)));
        etMax.setText(Integer.toString(sharedPreferences.getInt(MAX, 90)));

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String First       = etFirst.getText().toString();
                final String Second      = etSecond.getText().toString();
                final String Third       = etThird.getText().toString();
                final String Fourth      = etFourth.getText().toString();
                final String Drive       = etDrive.getText().toString();
                final String Reverse     = etReverse.getText().toString();
                final String Stop        = etStop.getText().toString();
                final String Left        = etLeft.getText().toString();
                final String Right       = etRight.getText().toString();
                final String Straight    = etStraight.getText().toString();
                final String LightsOff   = etLightsOff.getText().toString();
                final String LightsOn    = etLightsOn.getText().toString();
                final String DoorOpen    = etDoorOpen.getText().toString();
                final String DoorClose   = etDoorClose.getText().toString();
                final String HornOn      = etHornOn.getText().toString();
                final String HornOff     = etHornOff.getText().toString();
                final String Min         = etMin.getText().toString();
                final String Max         = etMax.getText().toString();

                if (Integer.parseInt(Max) > 180 || Integer.parseInt(Max) < 3) {
                    Toast.makeText(getApplicationContext(), "Invalid Value! Please enter from 3 - 180 degrees only.", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    editor.putString(FIRST, First).commit();
                    editor.putString(SECOND, Second).commit();
                    editor.putString(THIRD, Third).commit();
                    editor.putString(FOURTH, Fourth).commit();
                    editor.putString(DRIVE, Drive).commit();
                    editor.putString(REVERSE, Reverse).commit();
                    editor.putString(STOP, Stop).commit();
                    editor.putString(LEFT, Left).commit();
                    editor.putString(RIGHT, Right).commit();
                    editor.putString(STRAIGHT, Straight).commit();
                    editor.putString(LIGHTS_OFF, LightsOff).commit();
                    editor.putString(LIGHTS_ON, LightsOn).commit();
                    editor.putString(DOOR_OPEN, DoorOpen).commit();
                    editor.putString(DOOR_CLOSE, DoorClose).commit();
                    editor.putString(HORN_ON, HornOn).commit();
                    editor.putString(HORN_OFF, HornOff).commit();
                    editor.putInt(MAX, Integer.parseInt(Max)).commit();
                    servoPos = (int) Integer.parseInt(Max) / 2;
                    Toast.makeText(getApplicationContext(), "Saved! Make your servo Standby position to = " + servoPos, Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString(FIRST, "A").commit();
                editor.putString(SECOND, "B").commit();
                editor.putString(THIRD, "C").commit();
                editor.putString(FOURTH, "D").commit();
                editor.putString(DRIVE, "F").commit();
                editor.putString(REVERSE, "G").commit();
                editor.putString(STOP, "S").commit();
                editor.putString(LEFT, "L").commit();
                editor.putString(RIGHT, "R").commit();
                editor.putString(STRAIGHT, "I").commit();
                editor.putString(LIGHTS_OFF, "K").commit();
                editor.putString(LIGHTS_ON, "P").commit();
                editor.putString(DOOR_OPEN, "Q").commit();
                editor.putString(DOOR_CLOSE, "U").commit();
                editor.putString(HORN_ON, "H").commit();
                editor.putString(HORN_OFF, "J").commit();
                editor.putInt(MAX, 90).commit();
                Toast.makeText(getApplicationContext(), "Set to default values!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
