package com.josapedmoreno.androuino.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.josapedmoreno.androuino.R;
import com.josapedmoreno.androuino.connections.WifiConnections;

/**
 * Created by Josaped Moreno on 8/10/2015.
 */
public class WifiActivity extends Activity {
    // Tag for this Activity
    private static final String TAG = WifiActivity.class.getSimpleName();

    // Constant declaration
    private static final String PREF_IP = "PREF_IP_ADDRESS";
    private static final String PREF_PORT = "PREF_PORT_NUMBER";
    private Handler mHandler;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;

    WifiConnections connections;
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.wifi_setup);

        // Set result CANCELED in case the user backs out
        setResult(Activity.RESULT_CANCELED);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();

        final TextView txtAddress  = (TextView) findViewById(R.id.address);
        TextView txtPort           = (TextView) findViewById(R.id.port);
        final EditText ipAddress   = (EditText) findViewById(R.id.ipAddress);
        final EditText portAddress = (EditText) findViewById(R.id.portAddress);
        Button okButton            = (Button)   findViewById(R.id.button_ok);

        ipAddress.setText(sharedPreferences.getString(PREF_IP, "0.0.0.0"));
        portAddress.setText(sharedPreferences.getString(PREF_PORT, "8080"));

        okButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String Address = ipAddress.getText().toString();
                final String portNumber = portAddress.getText().toString();
                //Intent intent = new Intent(WifiActivity.this, FragmentWifi.class);
                //intent.putExtra(PREF_IP, Address);
                //intent.putExtra(PREF_PORT, portNumber);
                //startActivity(intent);

                if (Address.matches("") || portNumber.matches("")) {
                    Toast.makeText(getApplicationContext(), "Host IP / Port is empty", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    editor.putString(PREF_IP, Address);
                    editor.putString(PREF_PORT, portNumber);
                    editor.commit();
                    finish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
