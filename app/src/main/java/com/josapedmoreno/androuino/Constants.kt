package com.josapedmoreno.androuino

/**
 * Created by Josaped Moreno on 8/5/2015.
 * interfaces class should only contain abstract methods
 */
interface Constants {
    companion object {
        // Message types sent from the BluetoothChatService Handler
        val MESSAGE_STATE_CHANGE = 1
        val MESSAGE_READ = 2
        val MESSAGE_WRITE = 3
        val MESSAGE_DEVICE_NAME = 4
        val MESSAGE_TOAST = 5

        // Key names received from the BluetoothChatService Handler
        val DEVICE_NAME = "device_name"
        val TOAST = "toast"
    }


}
