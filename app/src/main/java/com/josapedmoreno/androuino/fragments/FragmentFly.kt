package com.josapedmoreno.androuino.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.Toast

import com.josapedmoreno.androuino.Constants
import com.josapedmoreno.androuino.R
import com.josapedmoreno.androuino.activity.BluetoothListActivity
import com.josapedmoreno.androuino.canvas.JoyStickClass
import com.josapedmoreno.androuino.connections.BluetoothConnections

/**
 * Created by Josaped Moreno on 8/30/2015.
 */
class FragmentFly : Fragment() {
    // views
    private lateinit var left_rear_wing: Button
    private lateinit var right_rear_wing: Button
    private lateinit var left_front_wing: Button
    private lateinit var right_front_wing: Button
    private lateinit var layoutJoyStick: RelativeLayout
    private lateinit var js: JoyStickClass

    /**
     * Name of the connected device
     */
    private var mConnectedDeviceName: String? = null

    /**
     * String buffer for outgoing messages
     */
    private val mOutStringBuffer: StringBuffer? = null

    /**
     * Local Bluetooth adapter
     */
    private var mBluetoothAdapter: BluetoothAdapter? = null

    /**
     * Member object for the BluetoothConnections services
     */
    private val mBTConnection: BluetoothConnections? = null

    /**
     * BroadcastReceiver declaration
     */
    internal var mmReceiver = mReciever()
    internal var btAdapterFilter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
    internal var wifiAdapterFiler = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)

    /**
     * The Handler that gets information back from the BluetoothConnections
     */
    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            val activity = activity
            when (msg.what) {
                Constants.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    BluetoothConnections.STATE_CONNECTED -> {
                    }
                    BluetoothConnections.STATE_CONNECTING -> {
                    }
                    BluetoothConnections.STATE_LISTEN -> {
                    }
                    BluetoothConnections.STATE_NONE -> {
                    }
                }
                Constants.MESSAGE_WRITE -> {
                    val writeBuf = msg.obj as ByteArray
                    // construct a string from the buffer
                    val writeMessage = String(writeBuf)
                }
                Constants.MESSAGE_READ -> {
                    val readBuf = msg.obj as ByteArray
                    // construct a string from the valid bytes in the buffer
                    val readMessage = String(readBuf, 0, msg.arg1)
                }
                Constants.MESSAGE_DEVICE_NAME -> {
                    // save the connected device's name
                    mConnectedDeviceName = msg.data.getString(Constants.DEVICE_NAME)
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to " + mConnectedDeviceName!!, Toast.LENGTH_SHORT).show()
                    }
                }
                Constants.MESSAGE_TOAST -> if (null != activity) {
                    Toast.makeText(activity, msg.data.getString(Constants.TOAST), Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            val activity = activity
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            activity!!.finish()
        }

        onNotification()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutJoyStick      = view.findViewById(R.id.layout_joystick) as RelativeLayout

        left_rear_wing      = view.findViewById(R.id.left_rear_wing) as Button
        right_rear_wing     = view.findViewById(R.id.right_rear_wing) as Button
        left_front_wing     = view.findViewById(R.id.left_front_wing) as Button
        right_front_wing    = view.findViewById(R.id.right_front_wing) as Button

        left_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
        left_front_wing.setBackgroundResource(R.drawable.propeller_idle)
        right_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
        right_front_wing.setBackgroundResource(R.drawable.propeller_idle)

        js = JoyStickClass(activity!!.applicationContext, layoutJoyStick, R.drawable.ic_drone)
        js.setStickSize(200, 200)
        js.setLayoutSize(800, 800)
        js.layoutAlpha = 150
        js.stickAlpha = 100
        js.offset = 90
        js.minimumDistance = 50

        layoutJoyStick.setOnTouchListener { _, event ->
            js.drawStick(event)
            if (event.action == MotionEvent.ACTION_DOWN || event.action == MotionEvent.ACTION_MOVE) {
                val direction = js.get8Direction
                when (direction) {
                    JoyStickClass.STICK_UP -> {
                        left_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_UPRIGHT -> {
                        right_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_RIGHT -> {
                        right_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_DOWNRIGHT -> {
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_DOWN -> {
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                    }
                    JoyStickClass.STICK_DOWNLEFT -> {
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_LEFT -> {
                        left_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_UPLEFT -> {
                        left_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                    JoyStickClass.STICK_NONE -> {
                        left_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_front_wing.setBackgroundResource(R.drawable.propeller_active)
                        left_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                        right_rear_wing.setBackgroundResource(R.drawable.propeller_active)
                    }
                }
            } else if (event.action == MotionEvent.ACTION_UP) {
                left_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                right_front_wing.setBackgroundResource(R.drawable.propeller_idle)
                left_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
                right_rear_wing.setBackgroundResource(R.drawable.propeller_idle)
            }
            true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_fly, container, false)
        return rootView
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CONNECT_DEVICE_SECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data!!, true)
                }
            REQUEST_CONNECT_DEVICE_INSECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data!!, false)
                }
            REQUEST_ENABLE_BT ->
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled
                    //sendCommand();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(activity, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show()
                    //getActivity().finish();
                }
        }
    }

    /**
     * Establish connection with other divice
     *
     * @param data   An [Intent] with [BluetoothListActivity.EXTRA_DEVICE_ADDRESS] extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private fun connectDevice(data: Intent, secure: Boolean) {
        // Get the device MAC address
        val address = data.extras!!.getString(BluetoothListActivity.EXTRA_DEVICE_ADDRESS)
        // Get the BluetoothDevice object
        val device = mBluetoothAdapter!!.getRemoteDevice(address)
        // Attempt to connect to the device
        mBTConnection!!.connect(device, secure)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.secure_connect_scan -> {
                if (mBluetoothAdapter!!.isEnabled) {
                    // Perform this
                    val intent = Intent(activity, BluetoothListActivity::class.java)
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE)

                    if (mBTConnection != null) {
                        // Only if the state is STATE_NONE, do we know that we haven't started already
                        if (mBTConnection.state == BluetoothConnections.STATE_NONE) {
                            // Start the Bluetooth send data services
                            mBTConnection.start()
                        }
                    }
                } else if (!mBluetoothAdapter!!.isEnabled) {
                    val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                    // Otherwise, setup the sendCommand method
                } else if (mBTConnection == null) {
                    //sendCommand();
                }// If BT is not on, request that it be enabled.
                // sendCommand() will then be called during onActivityResult
                return true
            }
        }/*case R.id.insecure_connect_scan: {
                // Launch the BluetoothListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), BluetoothListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }

            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }*/
        return false
    }

    override fun onStart() {
        //getActivity().registerReceiver(mmReceiver, btAdapterFilter);
        //getActivity().registerReceiver(mmReceiver, wifiAdapterFiler);
        super.onStart()
        // setup the sendCommand() method
        /*
        if (mBTConnection == null) {
            //sendCommand();
        }
        */
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        //getActivity().unregisterReceiver(mmReceiver);
        super.onDestroy()
        /*
        if (mBTConnection != null) {
            mBTConnection.stop();
        }
        */
    }

    /*private void sendCommand() {
        Log.d(TAG, "sendCommand");

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        imgButtonWifiNetworkIdle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WifiEnable();
            }
        });

        imgButtonBTNetworkIdle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mBluetoothAdapter.isEnabled()) {
                    // Perform this
                    Intent intent = new Intent(getActivity(), BluetoothListActivity.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);

                    if (mBTConnection != null) {
                        // Only if the state is STATE_NONE, do we know that we haven't started already
                        if (mBTConnection.getState() == BluetoothConnections.STATE_NONE) {
                            // Start the Bluetooth send data services
                            mBTConnection.start();
                        }
                    }
                }
                // If BT is not on, request that it be enabled.
                // sendCommand() will then be called during onActivityResult
                else if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                    // Otherwise, setup the sendCommand method
                } else if (mBTConnection == null) {
                    sendCommand();
                }
            }
        });

        // Initialize the BluetoothConnections to perform bluetooth connections
        mBTConnection = new BluetoothConnections(getActivity(), mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    private boolean WifiEnable() {
        WifiManager mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        if (!mWifiManager.isWifiEnabled()) {
            return mWifiManager.setWifiEnabled(true);
        }
        return false;
    }*/

    /**
     * BroadcastReceiver for checking if bluetooth in ON or OFF
     */
    inner class mReciever : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
                when (state) {
                    BluetoothAdapter.STATE_ON -> {
                        // Launch the BluetoothListActivity to see devices and do scan
                        val mIntent = Intent(activity, BluetoothListActivity::class.java)
                        startActivityForResult(mIntent, REQUEST_CONNECT_DEVICE_SECURE)
                    }

                    BluetoothAdapter.STATE_OFF ->
                        // if the user turned off the bluetooth, tops the connection from device
                        mBTConnection?.stop()
                }
            }

            val networkInfo = intent.getParcelableExtra<NetworkInfo>(ConnectivityManager.EXTRA_NETWORK_INFO)
            if (networkInfo.type == ConnectivityManager.TYPE_WIFI) {
                displayWifiName()
            }
        }
    }

    private fun displayWifiName() {
        val mConnectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        val mWifiManager = activity!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val mWifiInfo = mWifiManager.connectionInfo

        /*if (mNetworkInfo.isConnected()) {
            String ssid = mWifiInfo.getSSID();
            ssid = ssid.replaceAll("\"", "");
            txtWifiNetworkIdle.setText(ssid);
            txtWifiNetworkIdle.setTextColor(Color.rgb(0, 255, 0));
            imgButtonWifiNetworkIdle.setImageResource(R.drawable.ic_wifi_network_live);
        } else {
            txtWifiNetworkIdle.setText("");
            imgButtonWifiNetworkIdle.setImageResource(R.drawable.ic_wifi_network_idle);
        }*/
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private fun sendMessage(message: String) {
        // Check that we're actually connected before trying anything
        if (mBTConnection!!.state != BluetoothConnections.STATE_CONNECTED) {
            Toast.makeText(activity, R.string.not_connected, Toast.LENGTH_SHORT).show()
            return
        }

        // Check that there's actually something to send
        if (message.isNotEmpty()) {
            // Get the message bytes and tell the BluetoothConnections to write
            val send = message.toByteArray()
            mBTConnection.write(send)

            // Reset out string buffer to zero
            mOutStringBuffer!!.setLength(0)
        }
    }


    private fun onNotification() {
        // use the builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)

        builder.setTitle(R.string.title)
        builder.setMessage(R.string.set_message)

        builder.setPositiveButton(R.string.ok_button) { _, _ -> }
        builder.create()
        builder.show()
    }

    companion object {

        private val TAG = FragmentFly::class.java.simpleName

        // Intent request codes
        private val REQUEST_CONNECT_DEVICE_SECURE = 1
        private val REQUEST_CONNECT_DEVICE_INSECURE = 2
        private val REQUEST_ENABLE_BT = 3
    }
}
