package com.josapedmoreno.androuino.fragments

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.text.format.Formatter
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import android.widget.ToggleButton

import com.josapedmoreno.androuino.Constants
import com.josapedmoreno.androuino.R
import com.josapedmoreno.androuino.activity.WifiActivity
import com.josapedmoreno.androuino.connections.WifiConnections
import timber.log.Timber

import java.io.IOException
import java.io.OutputStream
import java.net.Socket
import java.net.UnknownHostException

/**
 * Created by Josaped Moreno on 8/31/2015.
 */
class FragmentWifi : Fragment(), View.OnClickListener {
    // views
    private lateinit var door: Button
    private lateinit var left: Button
    private lateinit var pedal: Button
    private lateinit var right: Button
    private lateinit var first: Button
    private lateinit var third: Button
    private lateinit var second: Button
    private lateinit var fourth: Button
    private lateinit var headlight: Button
    private lateinit var fragmentButton: Button
    private lateinit var driveReverse: ToggleButton
    private lateinit var txtStatus: TextView
    private lateinit var control: Switch
    private lateinit var sbPwm: SeekBar
    private lateinit var wheel: ImageView
    // double
    private var mCurrAngle = 0.0
    private var mPrevAngle = 0.0
    private var mGetCurrAngle = 0
    private var setRotate = 0
    // boolean
    private var isOn = false
    private val isChange = false
    // os
    lateinit var fade: Animation
    private var socket: Socket? = null
    private val mManager: WifiManager? = null
    private var wifiTCP: WifiConnections? = null
    private var mOutputStream: OutputStream? = null
    lateinit var sharedPreferences: SharedPreferences

    private var mmReceiver = Receiver()
    private var wifiAdapterFiler = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)

    private var runnable: Runnable = Runnable {
        /*
            if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") != "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") != "Port") {
                txtStatus.setText(sharedPreferences.getString("PREF_IP_ADDRESS", "Address") + ":" + sharedPreferences.getString("PREF_PORT_NUMBER", "Port"));
            }
            */
    }

    // check if device's wifi card is enable ro not
    private val isEnabled: Boolean
        get() {
            Timber.tag("isEnabled").d("Wifi is not enabled")
            val mWifiManager = activity!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
            val isWifiEnabled = mWifiManager.isWifiEnabled

            if (!isWifiEnabled) {
                if (socket != null) {
                    try {
                        socket!!.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }
                return false
            }
            return true
        }

    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            val activity = activity
            when (msg.what) {
                Constants.MESSAGE_WRITE -> {
                    val writeBuf = msg.obj as ByteArray
                    // construct a string from the buffer
                    val writeMessage = String(writeBuf)
                }
                Constants.MESSAGE_READ -> {
                    val readBuf = msg.obj as ByteArray
                    // construct a string from the valid bytes in the buffer
                    val readMessage = String(readBuf, 0, msg.arg1)
                }
            }//System.out.println(readMessage);
            //Toast.makeText(activity, readMessage, Toast.LENGTH_SHORT).show();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        txtStatus       = view.findViewById(R.id.txtStatus)         as TextView
        first           = view.findViewById(R.id.first)             as Button
        second          = view.findViewById(R.id.second)            as Button
        third           = view.findViewById(R.id.third)             as Button
        fourth          = view.findViewById(R.id.fourth)            as Button
        pedal           = view.findViewById(R.id.pedal)             as Button
        right           = view.findViewById(R.id.btnRight)          as Button
        left            = view.findViewById(R.id.btnLeft)           as Button
        headlight       = view.findViewById(R.id.headlight)         as Button
        door            = view.findViewById(R.id.door)              as Button
        driveReverse    = view.findViewById(R.id.driveReverse)      as ToggleButton
        control         = view.findViewById(R.id.control)           as Switch
        fragmentButton  = view.findViewById(R.id.fragmentSwitch)    as Button
        wheel           = view.findViewById(R.id.steer_wheel)       as ImageView

        control.textOn  = "Button"
        control.textOff = "Wheel"

        sbPwm.visibility    = View.INVISIBLE

        headlight.setBackgroundResource(R.drawable.headlightsoff)
        door.setBackgroundResource(R.drawable.door)
        fragmentButton.setBackgroundResource(R.drawable.ic_bluetooth)

        if (!control.isChecked) {
            right.visibility = View.INVISIBLE
            left.visibility = View.INVISIBLE
        }

        driveReverse.isChecked = true

        // add padding style for the gear buttons
        val firstGear = GradientDrawable()
        firstGear.shape = GradientDrawable.RECTANGLE
        firstGear.setStroke(5, Color.rgb(0, 0, 0))
        firstGear.setColor(Color.rgb(0, 255, 0))
        first.setBackgroundDrawable(firstGear)

        val secondGear = GradientDrawable()
        secondGear.shape = GradientDrawable.RECTANGLE
        secondGear.setStroke(5, Color.rgb(0, 0, 0))
        secondGear.setColor(Color.rgb(255, 0, 0))
        second.setBackgroundDrawable(secondGear)

        val thirdGear = GradientDrawable()
        thirdGear.shape = GradientDrawable.RECTANGLE
        thirdGear.setStroke(5, Color.rgb(0, 0, 0))
        thirdGear.setColor(Color.rgb(255, 0, 0))
        third.setBackgroundDrawable(thirdGear)

        val fourthGear = GradientDrawable()
        fourthGear.shape = GradientDrawable.RECTANGLE
        fourthGear.setStroke(5, Color.rgb(0, 0, 0))
        fourthGear.setColor(Color.rgb(255, 0, 0))
        fourth.setBackgroundDrawable(fourthGear)

        // Set volume Up key for activating Horn
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    val hornOn = sharedPreferences.getString(HORN_ON, "HORN_ON")!! + "\n"
                    if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                        Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                    } else {
                        sendMessage(hornOn)
                    }
                    return@OnKeyListener true
                } else if (event.action == KeyEvent.ACTION_UP) {
                    val hornOff = sharedPreferences.getString(HORN_OFF, "HORN_OFF")!! + "\n"
                    if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                        Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                    } else {
                        sendMessage(hornOff)
                    }
                    return@OnKeyListener true
                }
            }
            false
        })

        val seekBarHandler = Handler()

        seekBarHandler.post {
            sbPwm.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    Log.d(TAG, progress.toString())
                }

                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }
            })
        }
    }

    private fun sendCommand() {
        // instantiate a wifi class
        wifiTCP = WifiConnections()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)

        //Bundle bundle = new Bundle();
        //bundle = getActivity().getIntent().getExtras();
        //txtStatus.setText(bundle.getString("Address") + ":" + bundle.getString("portNumber"));

        first.setOnClickListener(this)
        second.setOnClickListener(this)
        third.setOnClickListener(this)
        fourth.setOnClickListener(this)

        pedal.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (driveReverse.isChecked) {
                    val drive = sharedPreferences.getString(DRIVE, "DRIVE")!! + "\n"
                    if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                        Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                    } else {
                        sendMessage(drive)
                    }
                } else {
                    val reverse = sharedPreferences.getString(REVERSE, "REVERSE")!! + "\n"
                    if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                        Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                    } else {
                        sendMessage(reverse)
                    }
                }
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                val stop = sharedPreferences.getString(STOP, "STOP")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(stop)
                }
            }
            false
        })

        right.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                val right = sharedPreferences.getString(RIGHT, "RIGHT")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(right)
                }
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                val straight = sharedPreferences.getString(STRAIGHT, "STRAIGHT")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(straight)
                }
            }
            false
        })

        left.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                val left = sharedPreferences.getString(LEFT, "LEFT")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(left)
                }
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                val straight = sharedPreferences.getString(STRAIGHT, "STRAIGHT")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(straight)
                }
            }
            false
        })

        wheel.setOnTouchListener { _, event ->
            val xc = (wheel.width / 2).toFloat()
            val yc = (wheel.height / 2).toFloat()

            val x = event.x
            val y = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    wheel.clearAnimation()
                }

                MotionEvent.ACTION_MOVE -> {
                    mPrevAngle = mCurrAngle
                    mCurrAngle = Math.toDegrees(Math.atan2((x - xc).toDouble(), (yc - y).toDouble()))
                    animate(mPrevAngle, mCurrAngle, 1000)
                }

                MotionEvent.ACTION_UP -> {
                    mCurrAngle = 0.0
                    mPrevAngle = mCurrAngle
                }
            }
            true
        }

        door.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                val doorOpen = sharedPreferences.getString(DOOR_OPEN, "DOOR_OPEN")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(doorOpen)
                }
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                val doorClose = sharedPreferences.getString(DOOR_CLOSE, "DOOR_CLOSE")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(doorClose)
                }
                return@OnTouchListener true
            }
            false
        })

        control.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // WHEEL
                wheel.clearAnimation()
                wheel.visibility = View.INVISIBLE
                // RIGHT
                if (right.visibility == View.INVISIBLE && left.visibility == View.INVISIBLE) {
                    right.clearAnimation()
                    right.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    right.startAnimation(fade)
                    // LEFT
                    left.clearAnimation()
                    left.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    left.startAnimation(fade)
                }
            } else {
                // WHEEL
                if (wheel.visibility == View.INVISIBLE) {
                    wheel.clearAnimation()
                    wheel.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    wheel.startAnimation(fade)
                }
                // RIGHT
                right.visibility = View.INVISIBLE
                // LEFT
                left.visibility = View.INVISIBLE
            }
        }

        fragmentButton.setOnClickListener {
            fragmentButton.setBackgroundResource(R.drawable.ic_wifi)
            val fragmentBluetooth = FragmentBluetooth()
            val fragmentManager = fragmentManager
            fragmentManager!!.beginTransaction().replace(R.id.frame_container, fragmentBluetooth).commit()
        }

        headlight.setOnClickListener {
            if (isOn) {
                headlight.setBackgroundResource(R.drawable.headlightsoff)
                val lightsOn = sharedPreferences.getString(LIGHTS_ON, "LIGHTS_ON")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(lightsOn)
                }
            } else {
                headlight.setBackgroundResource(R.drawable.headlightson)
                val lightsOff = sharedPreferences.getString(LIGHTS_OFF, "LIGHTS_OFF")!! + "\n"
                if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
                    Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
                } else {
                    sendMessage(lightsOff)
                }
            }

            isOn = !isOn
        }
    }

    private fun animate(fromDegrees: Double, toDegrees: Double, durationMillis: Long) {
        val rotate = RotateAnimation(fromDegrees.toFloat(), toDegrees.toFloat(), RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = durationMillis
        rotate.interpolator = LinearInterpolator()
        rotate.isFillEnabled = true
        rotate.fillAfter = true
        rotate.interpolator = LinearInterpolator()
        mGetCurrAngle = sharedPreferences.getInt(MAX, 0)
        setRotate = Math.round((mGetCurrAngle / 2 - mGetCurrAngle).toFloat())
        wheel.rotation = setRotate.toFloat()

        if (mCurrAngle <= mGetCurrAngle && mCurrAngle >= 0) {
            wheel.startAnimation(rotate)
            //System.out.println(Math.round(mCurrAngle));
            val send = Math.round(mCurrAngle).toString()
            val command = "<$send>"
            sendMessage(command)
        }
    }

    override fun onStart() {
        activity!!.registerReceiver(mmReceiver, wifiAdapterFiler)
        sendCommand()
        super.onStart()
    }

    override fun onResume() {
        /*
        if ((sharedPreferences.getString("PREF_IP_ADDRESS", "Address") == "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") == "Port") ||
                (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") == null && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") == null)) {
        } else {
        }
        */
        super.onResume()
        Thread(ClientThread()).start()
    }

    override fun onDestroy() {
        activity!!.unregisterReceiver(mmReceiver)
        super.onDestroy()
        if (socket != null) {
            try {
                socket!!.close()
                socket = null
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        // Stop method tracing that the activity started during onCreate()
        android.os.Debug.stopMethodTracing()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_wifi, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_wifi, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.wifi_tcp_connection -> {
                val intent = Intent(activity, WifiActivity::class.java)
                startActivityForResult(intent, 4)
            }
        }
        return false
    }

    override fun onClick(v: View) {
        val command: String

        val firstGear = GradientDrawable()
        firstGear.shape = GradientDrawable.RECTANGLE
        firstGear.setStroke(5, Color.rgb(0, 0, 0))
        firstGear.setColor(Color.rgb(0, 255, 0))
        first.setBackgroundDrawable(firstGear)

        val secondGear = GradientDrawable()
        secondGear.shape = GradientDrawable.RECTANGLE
        secondGear.setStroke(5, Color.rgb(0, 0, 0))
        secondGear.setColor(Color.rgb(255, 0, 0))
        second.setBackgroundDrawable(secondGear)

        val thirdGear = GradientDrawable()
        thirdGear.shape = GradientDrawable.RECTANGLE
        thirdGear.setStroke(5, Color.rgb(0, 0, 0))
        thirdGear.setColor(Color.rgb(255, 0, 0))
        third.setBackgroundDrawable(thirdGear)

        val fourthGear = GradientDrawable()
        fourthGear.shape = GradientDrawable.RECTANGLE
        fourthGear.setStroke(5, Color.rgb(0, 0, 0))
        fourthGear.setColor(Color.rgb(255, 0, 0))
        fourth.setBackgroundDrawable(fourthGear)

        if (v.id == first.id) {
            command = sharedPreferences.getString(FIRST, "FIRST")!! + "\n"

            firstGear.setColor(Color.rgb(0, 255, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        } else if (v.id == second.id) {
            command = sharedPreferences.getString(SECOND, "SECOND")!! + "\n"

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(0, 255, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        } else if (v.id == third.id) {
            command = sharedPreferences.getString(THIRD, "THIRD")!! + "\n"

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(0, 255, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        } else {
            command = sharedPreferences.getString(FOURTH, "FOURTH")!! + "\n"

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(0, 255, 0))
            fourth.setBackgroundDrawable(fourthGear)
        }

        // Send a message using ip address and port number
        if (sharedPreferences.getString("PREF_IP_ADDRESS", "Address") === "Address" && sharedPreferences.getString("PREF_PORT_NUMBER", "Port") === "Port") {
            Toast.makeText(activity, "IP or Port is unknown or failed to provide", Toast.LENGTH_SHORT).show()
        } else {
            sendMessage(command)
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private fun sendMessage(message: String) {
        val mNetworkInfo: NetworkInfo? = null
        val mConnectivityManager: ConnectivityManager? = null

        //final OutputStream mOutputStream = null;

        if (!isConnected(mNetworkInfo, mConnectivityManager)) {
            Toast.makeText(context, "Please connect to ESP first", Toast.LENGTH_SHORT).show()
            return
        }

        // Check that there's actually something to send
        if (message.isNotEmpty()) {
            // Get the message bytes and tell the BluetoothConnections to write
            val send = message.toByteArray()

            Thread(Runnable {
                activity!!.runOnUiThread {
                    if (socket != null) {
                        try {
                            mOutputStream = socket!!.getOutputStream()
                            mOutputStream!!.write(send)
                            mOutputStream!!.flush()
                        } catch (e: UnknownHostException) {
                            Toast.makeText(context, "Unknown IP/Port. Please check the connection!", Toast.LENGTH_SHORT).show()
                        } catch (e: IOException) {
                            Toast.makeText(context, "Cannot send command. Please check the connection!", Toast.LENGTH_SHORT).show()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {
                        Toast.makeText(context, "Error on connection!", Toast.LENGTH_SHORT).show()
                    }
                }
            }).start()
        }
    }

    internal inner class ClientThread : Runnable {

        override fun run() {
            try {
                socket = Socket(sharedPreferences.getString("PREF_IP_ADDRESS", "Address"), Integer.parseInt(sharedPreferences.getString("PREF_PORT_NUMBER", "Port")!!))
            } catch (e: UnknownHostException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /**
     * BroadcastReceiver for checking if Wifi is connected
     */
    inner class Receiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val networkInfo = intent.getParcelableExtra<NetworkInfo>(ConnectivityManager.EXTRA_NETWORK_INFO)
            networkInfo.state
            if (networkInfo.type == ConnectivityManager.TYPE_WIFI) {
                displayWifiName()
                isEnabled
            }
        }
    }

    // When connection is established, display the wifi's name
    private fun displayWifiName() {
        val mConnectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        @SuppressLint("WifiManagerLeak") val mWifiManager = activity!!.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val mWifiInfo = mWifiManager.connectionInfo

        if (mNetworkInfo.isConnected && mNetworkInfo.state == NetworkInfo.State.CONNECTED) {
            onResume()
            var ssid = mWifiInfo.ssid
            val ip = mWifiInfo.ipAddress
            //int port = mWifiInfo.
            val ipAddress = Formatter.formatIpAddress(ip)
            ssid = ssid.replace("\"".toRegex(), "")
            txtStatus.text = "$ssid       Client's IP Address: $ipAddress"
            txtStatus.setTextColor(Color.rgb(0, 255, 0))
        } else {
            Timber.tag("displayWifiName").d("Disconnected")
            txtStatus.text = "Disconnected"
            txtStatus.setTextColor(Color.rgb(255, 0, 0))
            if (socket != null) {
                try {
                    socket!!.close()
                    socket = null
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
            mWifiManager.reconnect()
        }
    }

    // return true if wifi connection is established
    private fun isConnected(networkInfo: NetworkInfo?, connectivityManager: ConnectivityManager?): Boolean {
        var mNetworkInfo = networkInfo
        var mConnectivityManager = connectivityManager
        mConnectivityManager = activity!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        mNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)

        return mNetworkInfo!!.isConnected
    }

    companion object {

        private val TAG = FragmentWifi::class.java.simpleName
        private val FIRST = "FIRST"
        private val SECOND = "SECOND"
        private val THIRD = "THIRD"
        private val FOURTH = "FOURTH"
        private val DRIVE = "DRIVE"
        private val REVERSE = "REVERSE"
        private val STOP = "STOP"
        private val LEFT = "LEFT"
        private val RIGHT = "RIGHT"
        private val STRAIGHT = "STRAIGHT"
        private val LIGHTS_ON = "LIGHTS_ON"
        private val LIGHTS_OFF = "LIGHTS_OFF"
        private val DOOR_OPEN = "DOOR_OPEN"
        private val DOOR_CLOSE = "DOOR_CLOSE"
        private val HORN_ON = "HORN_ON"
        private val HORN_OFF = "HORN_OFF"
        private val MIN = "MIN"
        private val MAX = "MAX"
    }
}