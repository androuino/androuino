package com.josapedmoreno.androuino.fragments

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.josapedmoreno.androuino.R

import java.io.InputStream

/**
 * Created by Josaped Moreno on 9/17/2015.
 */
class FragmentArduinoCode : Fragment() {

    private var txtCode: TextView? = null
    private var txtESP: TextView? = null
    private var txtServer: TextView? = null
    private var txtInstructions: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        txtCode = view.findViewById<View>(R.id.txtCode) as TextView
        txtESP = view.findViewById<View>(R.id.txtESP) as TextView
        txtServer = view.findViewById<View>(R.id.txtServer) as TextView
        txtInstructions = view.findViewById<View>(R.id.txtInstructions) as TextView
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_arduino_code, container, false)
        return rootView
    }

    override fun onStart() {
        readFile()
        super.onStart()
    }

    private fun readFile() {
        try {
            val res = resources
            var mInputStream = res.openRawResource(R.raw.code)
            val bytes = ByteArray(mInputStream.available())
            mInputStream.read(bytes)
            txtCode!!.text = String(bytes)

            mInputStream = res.openRawResource(R.raw.initlua)
            val initlua = ByteArray(mInputStream.available())
            mInputStream.read(initlua)
            txtESP!!.text = String(initlua)

            mInputStream = res.openRawResource(R.raw.serverlua)
            val serverlua = ByteArray(mInputStream.available())
            mInputStream.read(serverlua)
            txtServer!!.text = String(serverlua)

            mInputStream = res.openRawResource(R.raw.instructions)
            val instructions = ByteArray(mInputStream.available())
            mInputStream.read(instructions)
            txtInstructions!!.text = String(instructions)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
