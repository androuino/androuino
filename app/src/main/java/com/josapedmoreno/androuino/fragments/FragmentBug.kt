package com.josapedmoreno.androuino.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import com.josapedmoreno.androuino.R

/**
 * Created by Josaped Moreno on 9/17/2015.
 */
class FragmentBug : Fragment(), View.OnClickListener {
    // views
    private lateinit var send: Button
    private lateinit var email: EditText
    // strings
    private var getBody: String = ""
    private var getEmail: String = ""
    private var getSubject: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        send = view.findViewById<View>(R.id.btnSend) as Button
        email = view.findViewById<View>(R.id.email) as EditText

        send.setOnClickListener(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bug, container, false)
    }

    private fun getSendEmailIntent(context: Context?, email: String, subject: String, body: String) {
        val emailIntent = Intent(Intent.ACTION_SEND)
        try {
            //emailIntent.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
            emailIntent.type = "text/plain"

            // add the recipients
            //if (email != null) {
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
            //}
            //if (subject != null) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, arrayOf(subject))
            //}
            //if (body != null) {
            context!!.startActivity(Intent.createChooser(emailIntent, "Complete Action!"))
            //}
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onClick(v: View) {
        getEmail = email.text.toString()

        if (v.id == send.id) {
            getSendEmailIntent(context, getEmail, getSubject, getBody)
        }
    }

    companion object {

        private val TAG = FragmentBug::class.java.simpleName
    }
}
