/**
 * FragmentBluetooth.java by josaped moreno
 */

package com.josapedmoreno.androuino.fragments

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Button
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import android.widget.ToggleButton

import com.josapedmoreno.androuino.Constants
import com.josapedmoreno.androuino.R
import com.josapedmoreno.androuino.activity.BluetoothListActivity
import com.josapedmoreno.androuino.connections.BluetoothConnections
import com.josapedmoreno.androuino.utils.MyBroadcastReceiver

/**
 * Created by Josaped Moreno on 8/5/2015.
 */
class FragmentBluetooth : Fragment() {
    // views
    private lateinit var left: Button
    private lateinit var door: Button
    private lateinit var right: Button
    private lateinit var first: Button
    private lateinit var third: Button
    private lateinit var pedal: Button
    private lateinit var second: Button
    private lateinit var fourth: Button
    private lateinit var headlight: Button
    private lateinit var fragmentButton: Button
    private lateinit var driveReverse: ToggleButton
    private lateinit var txtStatus: TextView
    private lateinit var wheel: ImageView
    private lateinit var bask: ImageView
    private lateinit var control: Switch
    private lateinit var fade: Animation
    // double
    private var mCurrAngle = 0.0
    private var mPrevAngle = 0.0
    private var mGetCurrAngle = 0
    private var setRotate = 0
    // booleans
    private var isOn = false
    private val isChange = false
    // os
    private lateinit var sharedPreferences: SharedPreferences

    /**
     * Name of the connected device
     */
    private var mConnectedDeviceName: String? = null

    /**
     * String buffer for outgoing messages
     */
    private var mOutStringBuffer: StringBuffer? = null

    /**
     * Local Bluetooth adapter
     */
    private var mBluetoothAdapter: BluetoothAdapter? = null

    /**
     * Member object for the BluetoothConnections services
     */
    private var mBTConnection: BluetoothConnections? = null

    /**
     * BroadcastReceiver declaration
     */
    internal var mmReceiver = mReciever()
    internal var btAdapterFilter = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)

    internal var myBroadcastReceiver = MyBroadcastReceiver()

    /**
     * The Handler that gets information back from the BluetoothConnections
     */
    private val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            val activity = activity
            when (msg.what) {
                Constants.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    BluetoothConnections.STATE_CONNECTED -> {
                    }
                    BluetoothConnections.STATE_CONNECTING -> {
                    }
                    BluetoothConnections.STATE_LISTEN, BluetoothConnections.STATE_NONE -> {
                    }
                }
                Constants.MESSAGE_WRITE -> {
                    val writeBuf = msg.obj as ByteArray
                    // construct a string from the buffer
                    val writeMessage = String(writeBuf)
                }
                Constants.MESSAGE_READ -> {
                    val readBuf = msg.obj as ByteArray
                    // construct a string from the valid bytes in the buffer
                    val readMessage = String(readBuf, 0, msg.arg1)
                    println(readMessage)
                }
                Constants.MESSAGE_DEVICE_NAME -> {
                    // save the connected device's name
                    mConnectedDeviceName = msg.data.getString(Constants.DEVICE_NAME)
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to " + mConnectedDeviceName!!, Toast.LENGTH_SHORT).show()
                        txtStatus!!.text = mConnectedDeviceName!!.toString()
                        txtStatus!!.setTextColor(Color.rgb(0, 255, 0))
                    }
                }
                Constants.MESSAGE_TOAST -> if (null != activity) {
                    Toast.makeText(activity, msg.data.getString(Constants.TOAST), Toast.LENGTH_SHORT).show()
                    txtStatus!!.text = "Disconnected"
                    txtStatus!!.setTextColor(Color.rgb(255, 0, 0))
                }
            }//Toast.makeText(activity, readMessage, Toast.LENGTH_SHORT).show();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            val activity = activity
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            activity!!.finish()
        }
    }

    /*public void onCreateConnection() {
        // use the builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.Title);
        builder.setMessage(R.string.choose_type_of_connection);

        // WIFI OPTION
        builder.setPositiveButton(R.string.wifi_connection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), WifiActivity.class);
                startActivityForResult(intent, 4);

            }
        });
        // BLUETOOTH OPTION
        builder.setNegativeButton(R.string.bluetooth_connection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // if bluetooth is already enabled
                if (mBluetoothAdapter.isEnabled()) {
                    // Perform this
                    Intent intent = new Intent(getActivity(), BluetoothListActivity.class);
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE);

                    if (mBTConnection != null) {
                        // Only if the state is STATE_NONE, do we know that we haven't started already
                        if (mBTConnection.getState() == BluetoothConnections.STATE_NONE) {
                            // Start the Bluetooth send command services
                            mBTConnection.start();
                        }
                    }
                }
                // If BT is not on, request that it be enabled.
                // sendCommand() will then be called during onActivityResult
                else if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                    // Otherwise, setup the sendCommand method
                } else if (mBTConnection == null) {
                    sendCommand();
                }
            }
        });
        builder.create();
        builder.show();
    }*/

    /**
     * BroadcastReceiver for checking if bluetooth in ON or OFF
     */
    inner class mReciever : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val action = intent.action

            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
                when (state) {
                    BluetoothAdapter.STATE_ON -> {
                        // Launch the BluetoothListActivity to see devices and do scan
                        val mIntent = Intent(activity, BluetoothListActivity::class.java)
                        startActivityForResult(mIntent, REQUEST_CONNECT_DEVICE_SECURE)
                    }

                    BluetoothAdapter.STATE_OFF ->
                        // if the user turned off the bluetooth, tops the connection from device
                        if (mBTConnection != null) {
                            mBTConnection!!.stop()
                        }
                }
            }
            Log.d(TAG, "BroadcastReciever")
        }
    }

    override fun onStart() {
        activity!!.registerReceiver(mmReceiver, btAdapterFilter)
        activity!!.registerReceiver(myBroadcastReceiver, btAdapterFilter)
        super.onStart()
        // setup the sendCommand() method
        if (mBTConnection == null) {
            sendCommand()
        }
    }

    override fun onDestroy() {
        activity!!.unregisterReceiver(mmReceiver)
        activity!!.unregisterReceiver(myBroadcastReceiver)
        super.onDestroy()
        if (mBTConnection != null) {
            mBTConnection!!.stop()
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_bluetooth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Initializing the UI interfaces
        txtStatus       = view.findViewById<View>(R.id.txtStatus) as TextView
        first           = view.findViewById<View>(R.id.first) as Button
        second          = view.findViewById<View>(R.id.second) as Button
        third           = view.findViewById<View>(R.id.third) as Button
        fourth          = view.findViewById<View>(R.id.fourth) as Button
        pedal           = view.findViewById<View>(R.id.pedal) as Button
        right           = view.findViewById<View>(R.id.btnRight) as Button
        left            = view.findViewById<View>(R.id.btnLeft) as Button
        headlight       = view.findViewById<View>(R.id.headlight) as Button
        door            = view.findViewById<View>(R.id.door) as Button
        driveReverse    = view.findViewById<View>(R.id.driveReverse) as ToggleButton
        control         = view.findViewById<View>(R.id.control) as Switch
        fragmentButton  = view.findViewById<View>(R.id.fragmentSwitch) as Button
        wheel           = view.findViewById<View>(R.id.steer_wheel) as ImageView

        control.textOn = "Button"
        control.textOff = "Wheel"
        fragmentButton.setBackgroundResource(R.drawable.ic_wifi)

        headlight.setBackgroundResource(R.drawable.headlightsoff)
        door.setBackgroundResource(R.drawable.door)

        if (!control.isChecked) {
            right.visibility = View.INVISIBLE
            left.visibility = View.INVISIBLE
        }

        // set the toggle button to ON
        driveReverse.isChecked = true

        // add a padding style for the gear button
        val firstGear = GradientDrawable()
        firstGear.shape = GradientDrawable.RECTANGLE
        firstGear.setStroke(5, Color.rgb(0, 0, 0))
        firstGear.setColor(Color.rgb(0, 255, 0))
        first.setBackgroundDrawable(firstGear)

        val secondGear = GradientDrawable()
        secondGear.shape = GradientDrawable.RECTANGLE
        secondGear.setStroke(5, Color.rgb(0, 0, 0))
        secondGear.setColor(Color.rgb(255, 0, 0))
        second.setBackgroundDrawable(secondGear)

        val thirdGear = GradientDrawable()
        thirdGear.shape = GradientDrawable.RECTANGLE
        thirdGear.setStroke(5, Color.rgb(0, 0, 0))
        thirdGear.setColor(Color.rgb(255, 0, 0))
        third.setBackgroundDrawable(thirdGear)

        val fourthGear = GradientDrawable()
        fourthGear.shape = GradientDrawable.RECTANGLE
        fourthGear.setStroke(5, Color.rgb(0, 0, 0))
        fourthGear.setColor(Color.rgb(255, 0, 0))
        fourth.setBackgroundDrawable(fourthGear)

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                if (event.action == KeyEvent.ACTION_DOWN) {
                    val command = sharedPreferences.getString(HORN_ON, "HORN_ON")!! + "\n"
                    sendMessage(command)
                    return@OnKeyListener true
                } else if (event.action == KeyEvent.ACTION_UP) {
                    val command = sharedPreferences.getString(HORN_OFF, "HORN_OFF")!! + "\n"
                    sendMessage(command)
                    return@OnKeyListener true
                }
            }
            false
        })
    }

    /**
     * Set up the UI and background operations for sending command.
     */
    private fun sendCommand() {
        Log.d(TAG, "sendCommand()")

        val firstGear = GradientDrawable()
        firstGear.shape = GradientDrawable.RECTANGLE
        firstGear.setStroke(5, Color.rgb(0, 0, 0))
        firstGear.setColor(Color.rgb(0, 255, 0))
        first.setBackgroundDrawable(firstGear)

        val secondGear = GradientDrawable()
        secondGear.shape = GradientDrawable.RECTANGLE
        secondGear.setStroke(5, Color.rgb(0, 0, 0))
        secondGear.setColor(Color.rgb(255, 0, 0))
        second.setBackgroundDrawable(secondGear)

        val thirdGear = GradientDrawable()
        thirdGear.shape = GradientDrawable.RECTANGLE
        thirdGear.setStroke(5, Color.rgb(0, 0, 0))
        thirdGear.setColor(Color.rgb(255, 0, 0))
        third.setBackgroundDrawable(thirdGear)

        val fourthGear = GradientDrawable()
        fourthGear.shape = GradientDrawable.RECTANGLE
        fourthGear.setStroke(5, Color.rgb(0, 0, 0))
        fourthGear.setColor(Color.rgb(255, 0, 0))
        fourth.setBackgroundDrawable(fourthGear)

        // Initialize buttons with a listener that for click events
        first.setOnClickListener {
            // Send a message using content of the edit text widget
            val command = sharedPreferences.getString(FIRST, "FIRST")!! + "\n"
            sendMessage(command)

            firstGear.setColor(Color.rgb(0, 255, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        }

        second.setOnClickListener {
            val command = sharedPreferences.getString(SECOND, "SECOND")!! + "\n"
            sendMessage(command)

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(0, 255, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        }

        third.setOnClickListener {
            val command = sharedPreferences.getString(THIRD, "THIRD")!! + "\n"
            sendMessage(command)

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(0, 255, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(255, 0, 0))
            fourth.setBackgroundDrawable(fourthGear)
        }

        fourth.setOnClickListener {
            val command = sharedPreferences.getString(FOURTH, "FOURTH")!! + "\n"
            sendMessage(command)

            firstGear.setColor(Color.rgb(255, 0, 0))
            first.setBackgroundDrawable(firstGear)
            secondGear.setColor(Color.rgb(255, 0, 0))
            second.setBackgroundDrawable(secondGear)
            thirdGear.setColor(Color.rgb(255, 0, 0))
            third.setBackgroundDrawable(thirdGear)
            fourthGear.setColor(Color.rgb(0, 255, 0))
            fourth.setBackgroundDrawable(fourthGear)
        }

        pedal.setOnTouchListener(View.OnTouchListener { _, event ->
            val command: String
            if (event.action == MotionEvent.ACTION_DOWN) {
                if (driveReverse.isChecked) {
                    command = sharedPreferences.getString(DRIVE, "DRIVE")!! + "\n"
                    sendMessage(command)
                } else {
                    command = sharedPreferences.getString(REVERSE, "REVERSE")!! + "\n"
                    sendMessage(command)
                }
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                command = sharedPreferences.getString(STOP, "STOP")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            }
            false
        })

        right.setOnTouchListener(View.OnTouchListener { _, event ->
            val command: String
            if (event.action == MotionEvent.ACTION_DOWN) {
                command = sharedPreferences.getString(RIGHT, "RIGHT")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                command = sharedPreferences.getString(STRAIGHT, "STRAIGHT")!! + "I\n"
                sendMessage(command)
                return@OnTouchListener true
            }
            false
        })

        left.setOnTouchListener(View.OnTouchListener { _, event ->
            val command: String
            if (event.action == MotionEvent.ACTION_DOWN) {
                command = sharedPreferences.getString(LEFT, "LEFT")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                command = sharedPreferences.getString(STRAIGHT, "STRAIGHT")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            }
            false
        })

        door.setOnTouchListener(View.OnTouchListener { _, event ->
            val command: String
            if (event.action == MotionEvent.ACTION_DOWN) {
                command = sharedPreferences.getString(DOOR_OPEN, "DOOR_OPEN")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            } else if (event.action == MotionEvent.ACTION_UP) {
                command = sharedPreferences.getString(DOOR_CLOSE, "DOOR_CLOSE")!! + "\n"
                sendMessage(command)
                return@OnTouchListener true
            }
            false
        })

        control.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                // WHEEL
                wheel.clearAnimation()
                wheel.visibility = View.INVISIBLE
                // RIGHT
                if (right.visibility == View.INVISIBLE && left.visibility == View.INVISIBLE) {
                    right.clearAnimation()
                    right.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    right.startAnimation(fade)
                    // LEFT
                    left.clearAnimation()
                    left.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    left.startAnimation(fade)
                }
            } else {
                // WHEEL
                if (wheel.visibility == View.INVISIBLE) {
                    wheel.clearAnimation()
                    wheel.visibility = View.VISIBLE
                    fade = AnimationUtils.loadAnimation(context, R.anim.animation_fade)
                    wheel.startAnimation(fade)
                }
                // RIGHT
                right.visibility = View.INVISIBLE
                // LEFT
                left.visibility = View.INVISIBLE
            }
        }

        fragmentButton.setOnClickListener {
            fragmentButton.setBackgroundResource(R.drawable.ic_bluetooth)
            val fragmentWifi = FragmentWifi()
            val fragmentManager = fragmentManager
            fragmentManager!!.beginTransaction().replace(R.id.frame_container, fragmentWifi).commit()
        }

        wheel.setOnTouchListener { _, event ->
            val xc = (wheel.width / 2).toFloat()
            val yc = (wheel.height / 2).toFloat()

            val x = event.x
            val y = event.y

            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    wheel.clearAnimation()
                }

                MotionEvent.ACTION_MOVE -> {
                    mPrevAngle = mCurrAngle
                    mCurrAngle = Math.toDegrees(Math.atan2((x - xc).toDouble(), (yc - y).toDouble()))
                    animate(mPrevAngle, mCurrAngle, 1000)
                }

                MotionEvent.ACTION_UP -> {
                    mCurrAngle = 0.0
                    mPrevAngle = mCurrAngle
                }
            }
            true
        }

        headlight.setOnClickListener {
            val command: String
            if (isOn) {
                headlight!!.setBackgroundResource(R.drawable.headlightsoff)
                command = sharedPreferences.getString(LIGHTS_ON, "LIGHTS_ON")!! + "\n"
                sendMessage(command)
            } else {
                headlight!!.setBackgroundResource(R.drawable.headlightson)
                command = sharedPreferences.getString(LIGHTS_OFF, "LIGHTS_OFF")!! + "\n"
                sendMessage(command)
            }

            isOn = !isOn
        }

        // Initialize the BluetoothConnections to perform bluetooth connections
        mBTConnection = BluetoothConnections(activity, mHandler)

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = StringBuffer("")
    }

    private fun animate(fromDegrees: Double, toDegrees: Double, durationMillis: Long) {
        val rotate = RotateAnimation(fromDegrees.toFloat(), toDegrees.toFloat(), RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = durationMillis
        rotate.isFillEnabled = true
        rotate.fillAfter = true
        rotate.interpolator = LinearInterpolator()
        mGetCurrAngle = sharedPreferences.getInt(MAX, 0)
        setRotate = Math.round((mGetCurrAngle / 2 - mGetCurrAngle).toFloat())
        wheel!!.rotation = setRotate.toFloat()

        if (mCurrAngle <= mGetCurrAngle && mCurrAngle >= 0) {
            wheel!!.startAnimation(rotate)
            //System.out.println(Math.round(mCurrAngle));
            //int send = (int) Math.round(mCurrAngle);
            //int command = send;
            val command = "<" + Math.round(mCurrAngle).toString() + ">"
            Log.d(TAG, command)
            sendMessage(command)
        }
    }

    /**
     * Makes this device discoverable.
     * Disabled for future use
     */
    private fun ensureDiscoverable() {
        if (mBluetoothAdapter!!.scanMode != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300)
            startActivity(discoverableIntent)
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private fun sendMessage(message: String) {
        // Check that we're actually connected before trying anything
        if (mBTConnection!!.state != BluetoothConnections.STATE_CONNECTED) {
            Toast.makeText(activity, R.string.not_connected, Toast.LENGTH_SHORT).show()
            return
        }

        // Check that there's actually something to send
        if (message.length > 0) {
            // Get the message bytes and tell the BluetoothConnections to write
            val send = message.toByteArray()
            mBTConnection!!.write(send)

            // Reset out string buffer to zero
            mOutStringBuffer!!.setLength(0)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CONNECT_DEVICE_SECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data!!, true)
                }
            REQUEST_CONNECT_DEVICE_INSECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data!!, false)
                }
            REQUEST_ENABLE_BT ->
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled
                    sendCommand()
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(activity, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show()
                    //getActivity().finish();
                }
        }
    }

    /**
     * Establish connection with other divice
     *
     * @param data   An [Intent] with [BluetoothListActivity.EXTRA_DEVICE_ADDRESS] extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private fun connectDevice(data: Intent, secure: Boolean) {
        // Get the device MAC address
        val address = data.extras!!.getString(BluetoothListActivity.EXTRA_DEVICE_ADDRESS)
        // Get the BluetoothDevice object
        val device = mBluetoothAdapter!!.getRemoteDevice(address)
        // Attempt to connect to the device
        mBTConnection!!.connect(device, secure)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.menu_bluetooth, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.secure_connect_scan -> {
                if (mBluetoothAdapter!!.isEnabled) {
                    // Perform this
                    val intent = Intent(activity, BluetoothListActivity::class.java)
                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE)

                    if (mBTConnection != null) {
                        // Only if the state is STATE_NONE, do we know that we haven't started already
                        if (mBTConnection!!.state == BluetoothConnections.STATE_NONE) {
                            // Start the Bluetooth send data services
                            mBTConnection!!.start()
                        }
                    }
                } else if (!mBluetoothAdapter!!.isEnabled) {
                    val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                    // Otherwise, setup the sendCommand method
                } else if (mBTConnection == null) {
                    sendCommand()
                }// If BT is not on, request that it be enabled.
                // sendCommand() will then be called during onActivityResult
                return true
            }
        }/*case R.id.insecure_connect_scan: {
                // Launch the BluetoothListActivity to see devices and do scan
                Intent serverIntent = new Intent(getActivity(), BluetoothListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);
                return true;
            }

            case R.id.discoverable: {
                // Ensure this device is discoverable by others
                ensureDiscoverable();
                return true;
            }*/
        return false
    }

    companion object {

        private val TAG = FragmentBluetooth::class.java.simpleName

        // Intent request codes
        private val REQUEST_CONNECT_DEVICE_SECURE = 1
        private val REQUEST_CONNECT_DEVICE_INSECURE = 2
        private val REQUEST_ENABLE_BT = 3

        private val FIRST = "FIRST"
        private val SECOND = "SECOND"
        private val THIRD = "THIRD"
        private val FOURTH = "FOURTH"
        private val DRIVE = "DRIVE"
        private val REVERSE = "REVERSE"
        private val STOP = "STOP"
        private val LEFT = "LEFT"
        private val RIGHT = "RIGHT"
        private val STRAIGHT = "STRAIGHT"
        private val LIGHTS_ON = "LIGHTS_ON"
        private val LIGHTS_OFF = "LIGHTS_OFF"
        private val DOOR_OPEN = "DOOR_OPEN"
        private val DOOR_CLOSE = "DOOR_CLOSE"
        private val HORN_ON = "HORN_ON"
        private val HORN_OFF = "HORN_OFF"
        private val MIN = "MIN"
        private val MAX = "MAX"
    }
}