package com.josapedmoreno.androuino.utils

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created by josapedmoreno on 2017/07/02.
 */

class MyBroadcastReceiver : BroadcastReceiver() {
    private val TAG = "MyBroadcastReceiver"
    private val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    override fun onReceive(context: Context, intent: Intent) {
        val action = intent.action

        val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

        when (action) {
            BluetoothDevice.ACTION_FOUND -> {
            }
            BluetoothDevice.ACTION_ACL_CONNECTED -> Log.d(TAG, "Connected!")
            BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
            }
            BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED -> {
            }
            BluetoothDevice.ACTION_ACL_DISCONNECTED -> Log.d(TAG, "Disconnected!")
        }
    }
}
