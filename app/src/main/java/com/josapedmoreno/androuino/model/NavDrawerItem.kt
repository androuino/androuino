package com.josapedmoreno.androuino.model

/**
 * Created by Josaped Moreno on 8/28/2015.
 */
class NavDrawerItem {
    var title: String? = null
    var icon: Int = 0

    constructor() {}

    constructor(title: String, icon: Int) {
        this.title = title
        this.icon = icon
    }
}
