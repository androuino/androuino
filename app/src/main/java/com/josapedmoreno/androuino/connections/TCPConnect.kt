package com.josapedmoreno.androuino.connections

import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Handler

/**
 * Created by josapedmoreno on 5/9/16.
 */
class TCPConnect(private val mHandler: Handler) : AsyncTask<String, Void, ClientSocket>() {
    private val clientSocket: ClientSocket? = null
    internal var sharedPreferences: SharedPreferences? = null

    override fun doInBackground(vararg params: String): ClientSocket? {
        //clientSocket = new ClientSocket(mHandler, sharedPreferences.getString("PREF_IP_ADDRESS", "Address"), Integer.parseInt(sharedPreferences.getString("PREF_PORT_NUMBER", "Port")));
        return null
    }

    companion object {

        private val TAG = TCPConnect::class.java.simpleName
    }
}
