package com.josapedmoreno.androuino.connections

import android.content.Context
import android.os.AsyncTask
import android.os.Handler
import android.util.Log
import android.widget.Toast

import java.io.BufferedWriter
import java.io.DataInputStream
import java.io.DataOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.io.PrintWriter
import java.net.Socket
import java.net.UnknownHostException

/**
 * Created by Josaped Moreno on 9/12/2015.
 */
class WifiConnections {
    // os
    private var socket: Socket? = null
    private lateinit var out: PrintWriter
    private val mInputStream: DataInputStream? = null
    private var mOutputStream: DataOutputStream? = null
    // boolean
    private var connected: Boolean = false

    init {
        /*socket = null
        out = null
        connected = false*/
    }

    /*fun connect(context: Context, host: String, port: Int, command: String) {
        ConnectTask(context, host, port, command).execute(host, port.toString())
    }

    inner class ConnectTask(private val context: Context, private var host: String?, private var port: Int, private val command: String) : AsyncTask<String, Void, Void>() {

        override fun onPreExecute() {
            //showToast(context, "Connecting...");
            super.onPreExecute()
        }

        override fun onPostExecute(result: Void) {
            if (connected) {
                //showToast(context, "Connection Successful!");
            }
            super.onPostExecute(result)
        }

        override fun doInBackground(vararg params: String): Void? {
            try {
                host = params[0]
                port = Integer.parseInt(params[1])
                socket = Socket(host, port)
                connected = true
                //mInputStream = new DataInputStream(socket.getInputStream());
                mOutputStream = DataOutputStream(socket!!.getOutputStream())
                mOutputStream!!.write(command.toByteArray())
                mOutputStream!!.flush()
                mOutputStream!!.close()
                val out = PrintWriter(BufferedWriter(OutputStreamWriter(socket!!.getOutputStream())), true)
                out.println(command.toByteArray())
                out.flush()
                socket!!.close()
            } catch (e: UnknownHostException) {
                showToast(context, "Cannot connect to $host:$port")
                Log.e(TAG, e.message)
            } catch (e: IOException) {
                showToast(context, "Couldn't get I/O for the connection to: $host:$port")
                Log.e(TAG, e.message)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        fun send(command: String) {
            if (connected) {
                try {
                    println("Command sent!")
                    mOutputStream!!.write(command.toByteArray())
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun showToast(context: Context, message: String) {
        Handler(context.mainLooper).post { Toast.makeText(context, message, Toast.LENGTH_LONG).show() }
    }*/

    companion object {

        private val TAG = WifiConnections::class.java.simpleName
    }
}
