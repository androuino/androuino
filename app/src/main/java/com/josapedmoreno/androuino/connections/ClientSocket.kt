package com.josapedmoreno.androuino.connections

import android.content.Context
import android.content.SharedPreferences
import android.os.Handler
import android.util.Log

import com.josapedmoreno.androuino.Constants

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.net.UnknownHostException
import java.util.ArrayList

/**
 * Created by josapedmoreno on 5/12/16.
 */
class ClientSocket(context: Context, private val mHandler: Handler) {
    private val ipNumber: String? = null
    private val port: Int = 0
    private val running = false
    private var connected: Connected? = null
    private var connecting: Connecting? = null
    internal var sharedPreferences: SharedPreferences? = null

    @Synchronized
    fun start() {
        connecting = Connecting()
        connecting!!.start()
    }

    @Synchronized
    fun connected() {
        connected = Connected()
        connected!!.start()
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     * @see Connected.write
     */
    fun write(out: ByteArray) {
        // Create temporary object
        val r: Connected?
        // Synchronize a copy of the ConnectedThread
        synchronized(this) {
            r = connected
        }
        // Perform the write unsynchronized
        r!!.write(out)
    }

    inner class Connecting : Thread() {
        private var mSocket: Socket? = null

        override fun run() {
            // Connect to Wifi socket
            try {
                mSocket = Socket(sharedPreferences!!.getString("PREF_IP_ADDRESS", "Address"), Integer.parseInt(sharedPreferences!!.getString("PREF_PORT_NUMBER", "Port")!!))
            } catch (e: UnknownHostException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // Run Connected Thread
            //if (running) {
            connected()
            //}
        }

        fun cancel() {
            try {
                mSocket!!.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    inner class Connected : Thread() {
        private val mSocket: Socket? = null
        private val mInputStream: InputStream?
        private val mOutputStream: OutputStream?

        init {
            var tmIn: InputStream? = null
            var tmOut: OutputStream? = null

            try {
                tmIn = mSocket!!.getInputStream()
                tmOut = mSocket.getOutputStream()
            } catch (e: IOException) {
                e.printStackTrace()
            }

            mInputStream = tmIn
            mOutputStream = tmOut
        }

        override fun run() {
            Log.i(TAG, "BEGIN mConnectedThread")
            //byte[] buffer = new byte[1024];
            var buffer: ByteArray
            var bytes: Int
            var arr_byte = ArrayList<Int>()

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    // Read from the InputStream
                    //bytes = mmInStream.read(buffer);
                    bytes = mInputStream!!.read()
                    if (bytes == 0x0A) {
                    } else if (bytes == 0x0D) {
                        buffer = ByteArray(arr_byte.size)
                        for (i in arr_byte.indices) {
                            buffer[i] = arr_byte[i].toByte()
                        }
                        // Send the obtained bytes to the UI Activity
                        mHandler.obtainMessage(Constants.MESSAGE_READ, buffer.size, -1, buffer).sendToTarget()
                        arr_byte = ArrayList()
                    } else {
                        arr_byte.add(bytes)
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "disconnected", e)
                }

            }

        }

        /**
         * Write to the connected OutStream.
         *
         * @param buffer The bytes to write
         */
        fun write(buffer: ByteArray) {
            try {
                mOutputStream!!.write(buffer)
                mOutputStream.flush()
                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer).sendToTarget()
            } catch (e: IOException) {
                Log.e(TAG, "Exception during write", e)
            }

        }

        fun cancel() {
            try {
                mInputStream!!.close()
                mOutputStream!!.close()
                mSocket!!.close()
            } catch (e: IOException) {
                Log.e(TAG, "close() of connect socket failed", e)
            }

        }
    }

    companion object {

        private val TAG = ClientSocket::class.java.simpleName
    }
}
